package core;


import java.util.Objects;

public class SubArray {

    public int maxProduct(int[] array) {

        Objects.requireNonNull(array);
        if (array.length == 0) {
            return 0;
        }

        int maxSum = array[0];
        int sum = 0;

        for (int number : array) {
            sum += number;
            if (number < 0) {
                sum = 0;
            }
            maxSum = Math.max(maxSum, sum);
            sum = Math.max(sum, 0);
        }
        return maxSum;
    }

    public int[] maxSubArray(int[] array) {

        Objects.requireNonNull(array);
        if (array.length == 0) {
            return new int[0];
        }

        int maxSum = array[0];
        int sum = 0;
        int maxSumFirstElement = 0;
        int maxSumLastElement = 0;
        int minusPosition = -1;
        for (int i=0; i<array.length; ++i) {
            sum += array[i];

            if (array[i] < 0) {
                sum = 0;
                maxSumFirstElement=i;
                minusPosition = i;
            }
            if (sum > maxSum) {
                maxSum = sum;
                maxSumFirstElement = minusPosition +1;
                maxSumLastElement = i;
            }
        }

        int[] subArray = new int[maxSumLastElement-maxSumFirstElement+1];

        for (int i = 0; i < subArray.length; i++) {
            subArray[i] = array[maxSumFirstElement++];
        }

        return subArray;
    }
}
