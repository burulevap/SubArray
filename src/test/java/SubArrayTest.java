import core.SubArray;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class SubArrayTest {

    SubArray subArray = new SubArray();
    private int[] testArray;
    private int[] testSubArray;

    @Before
    public void prepareData() {
        testArray = new int[]{2, 3, -3, 4, 5, 7};
        testSubArray = new int[]{4, 5, 7};
    }

    @Test
    public void testMaxProduct() {
        assertEquals(subArray.maxProduct(testArray), 16);
    }

    @Test
    public void testEmptyInputMaxProduct() {

        assertEquals(subArray.maxProduct(new int[0]), 0);
    }

    @Test
    public void testEmptyInputMaxSubArray() {

        assertArrayEquals(subArray.maxSubArray(new int[0]), new int[0]);
    }

    @Test
    public void testMaxSubArray() {
        assertArrayEquals(subArray.maxSubArray(testArray), testSubArray);
    }
}
